from bgmd import Bgmd, Md2HtmlConverter, MD_FLAVOR_BEET, HtmlTagStyle
import gongsf as gsf

markdown = Md2HtmlConverter(MD_FLAVOR_BEET)

schmea_file = open('gsf/header.gts')
schema_header = gsf.codec.read_schema_txt(schmea_file.read())[1]
schmea_file.close()

def set_headers(id: str) -> None:
	for i in range(6):
		markdown.custom_tags[f'h{i + 1}'] = HtmlTagStyle(f'h{i + 1}', attributes={'id': id})

def reset_headers() -> None:
	for i in range(6):
		if f'h{i + 1}' not in markdown.custom_tags:
			continue
		del markdown.custom_tags[f'h{i + 1}']

header_tags: list[str] | None = None
header_subtitle: str | None = None

def on_gsf(string: str) -> None:
	global header_tags
	global header_subtitle
	article = gsf.codec.read_article_txt(string, schema_header)
	gsf_header = article.root.decode_object()
	header_tags = []
	for tag in gsf_header['tags'].decode_list().string:
		header_tags.append(tag)
	if gsf_header['id'].type.is_string():
		set_headers(gsf_header['id'].string)
	if gsf_header['subtitle'].type.is_string():
		header_subtitle = gsf_header['subtitle'].string

def convert(md: str) -> str:
	global header_tags
	global header_subtitle
	markdown.put_md(md)
	while not markdown.done():
		if block := markdown.check():
			if block[0] == 'gsf':
				on_gsf(block[1])
			continue
		markdown.line()
		if header_tags is not None:
			if header_subtitle is not None:
				markdown.tag_open('p', class_='subtitle')
				markdown.html += header_subtitle
				markdown.tag_close('p')
			markdown.tag_open('div', class_='header-sep')
			markdown.tag_empty('hr')
			for tag in header_tags:
				markdown.tag_open('span')
				markdown.html += tag
				markdown.tag_close('span')
			markdown.tag_close('div')
			markdown.html += '\n'
			header_tags = None
			header_subtitle = None
			reset_headers()
	return markdown.pull_html()

Bgmd('out', 'src/template.html', 'src', convert).run()
