!gsf {
@macol-devblog:header:0
subtitle: introduction to the project and current progress
tags:
  : concept
  : wiring
  : code
}
# Beetwatch Update #0

!gsf {
@macol-devblog:header:0
tags:
  : code
id: 'display-code'
}
## Programming the display

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed erat
vitae odio consequat egestas vitae vitae turpis. Pellentesque risus
neque, imperdiet nec massa vel, facilisis maximus mauris. Etiam
elementum lacinia velit, in porttitor nibh suscipit eu. Vestibulum
ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
curae; Donec nec nisl quam. Sed maximus magna tellus, commodo suscipit
elit ultricies sit amet. Aliquam posuere suscipit sapien, eu vulputate
nisi vulputate et. Nulla tristique aliquam sem ornare pharetra. Donec
at efficitur sem. Fusce fermentum, sem at eleifend condimentum, elit
nibh gravida tortor, sit amet hendrerit nunc libero vel dolor.

Maecenas et purus ac nisl iaculis fermentum ut eget felis. Fusce iaculis a ligula quis tristique. Fusce in rutrum ipsum. Maecenas eget quam in nisl molestie aliquam sit amet vel augue. Vivamus lobortis dolor vel porttitor ullamcorper. Donec condimentum dui eu lectus convallis, ac dictum dolor finibus. Quisque sit amet nisi id velit viverra iaculis. Nulla et ullamcorper metus. Curabitur mattis condimentum massa, at eleifend felis lobortis a.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec metus ac odio rutrum ultrices eu sed neque. Morbi facilisis iaculis neque. Maecenas laoreet sem nunc, id auctor augue luctus at. Morbi eget sem rhoncus lectus gravida imperdiet id non lectus. Vestibulum finibus nisi non leo pretium, sed sagittis nisl congue. Donec interdum nisl id enim dictum, a consectetur libero condimentum. Aenean eget dui dui. Nam iaculis augue in elit auctor tincidunt. Etiam aliquam turpis porta lacus molestie, in volutpat nulla suscipit. Phasellus vel lectus erat. 
